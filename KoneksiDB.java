/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package koleksibukuku;

/**
 *
 * @author windows10
 */

import java.sql.*;
import javax.swing.*;
import static javax.swing.JOptionPane.showMessageDialog;

public class KoneksiDB {
    public static String username;
    public static String password;
    ResultSet rs;
    
    public static Connection KoneksiDB(){
        String path="jdbc:sqlite:D:\\semester 5\\RPL\\KoleksiBukuku\\sqlitestudio-3.1.1\\SQLiteStudio\\RPL\\koleksibukuku.db";
        Connection con=null;
        try{
            con=DriverManager.getConnection(path);
        }
        catch(SQLException e){
            showMessageDialog(null,"Koneksi ke database gagal!","Error",JOptionPane.ERROR_MESSAGE);
        }
    
        return con;
    
    } 
}
